const Card = (props) => {
    const {question,answers} = props
    return(
        <>
        <h2 className="test-header">{question}</h2>
        <ul className="button-container">
          <li className="button-option">{answers[0].text}</li>
          <li className="button-option">{answers[1].text}</li>
          <li className="button-option">{answers[2].text}</li>
          <li className="button-option">{answers[3].text}</li>
        </ul> 
        </>
    )
}
export default Card