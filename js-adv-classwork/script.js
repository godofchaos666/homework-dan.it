function makeCounter() {
    let counter = 0
    return function() {
        counter++
        console.log(counter);
    }
}

const incrementCounter1 = makeCounter()
console.log(incrementCounter1);
incrementCounter1()
incrementCounter1()
incrementCounter1()