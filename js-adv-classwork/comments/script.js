

const BASE_URL = 'https://ajax.test-danit.com/api/json'

const makeRequest = url => fetch(url).then((response) => {
    if (response.ok) {
        return response.json()
    } else {
        // console.log(response.er);
        throw new Error('Server Error')
    }
})

const getPost = id => makeRequest(`${BASE_URL}/posts/${id}`)
const getCommentsByPostId = id => makeRequest(`${BASE_URL}/posts/${id}/comments`)

class Element {
    createElement({tagName = 'div', classNames, text = ''}) {
        const element = document.createElement(tagName);
        element.classList.add(classNames)
        element.innerText = text;
        return element;
    }
}

class Post extends Element {
    constructor(title, body) {
        super(title, body);
        this.title = title;
        this.body = body;
    }

    render() {
        const div = this.createElement({ classNames: ['card-body']})
        const titleEl = this.createElement({
            tagName: 'h3',
            classNames: ['title'],
            text: this.title
        });
        const body = this.createElement({
            classNames: ['card-body'],
            text: this.body
        });
        div.append(titleEl, body);
        return div
    }
}

class Comment extends Post {
    constructor(name, body) {
        super(body);
        this.name = name;
        this.body = body;
    }
    renderComment(){
   
    }
}
let commentText = document.createElement('p')
const postSelect = document.querySelector('#post');
const cardEl = document.querySelector('.card');
const showComment = document.getElementById('show-comments')


const show = document.getElementById('show');    
showComment.addEventListener('click',function(){
    fetch(`${BASE_URL}/posts/${postSelect.value}/comments`)
    .then(response =>  response.json())
    .then(comments => {
    comments.forEach(comment => {
      const  {name,body} = comment
     
    
        const commentTitle = document.createElement('h2')
        commentTitle.classList.add('comment-title');
        const commentBody = document.createElement('p')
        commentBody.classList.add('comment-body');
        commentTitle.textContent =`Title: ${name}`
        commentBody.textContent = `Body: ${body}}`
       document.body.append(commentTitle)
       document.body.append(commentBody);
       showComment.remove()

     
    });

  
  })
})

   
postSelect.addEventListener('change', event => {
    let commentTitles = document.querySelectorAll('.comment-title');
let commentBodys = document.querySelectorAll('.comment-body');    

commentTitles.forEach(el => {
    el.textContent = ''
})
commentBodys.forEach(el => {
    el.textContent = ''
})
    document.body.append(showComment);
    const postId = event.target.value;
    cardEl.innerHTML = "";
    getPost(postId)
        .then(post => {
      
            const postObj = new Post(post.title, post.body)
          
            
            cardEl.append(postObj.render());

    })
   
})
        