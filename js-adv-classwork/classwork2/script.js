// // // /**
// // //  * Відрефакторити код позбавившись від дублювання коду
// // //  * 
// // //  * Для цього створити функцію createZoom, яка буде приймати назву селектора
// // //  * (selector) та значення зуму (zoomValue)
// // //  * 
// // //  * ADVANCED:
// // //  * Після створення зуму дати можливість викликати цю функцію, щоб дізнатися її
// // //  * параметри та елемент
// // //  * 
// // //  * Наприклад, 
// // //  * const zoomOut = createZoom('#zoom-out', 0.8);
// // //  * console.log(zoomOut()) // { zoom: 0.8,  }
// // // //  */
// // // window.addEventListener('click',function(){
// // // const fontSize = getComputedStyle(document.body).fontSize;
// // // const fontSizeValue = parseInt(fontSize);
// // //  document.body.style.fontSize = `${fontSizeValue * event.target.textContent.substr(1)}px`; 
// // // // })

// // // /**
// // //  * Написати функцію валідації поштової пошти
// // //  * - не менше 5 символів
// // //  * - наявність @
// // //  * - закінчується на .com
// // //  * 
// // //  * Прокидувати помилку для кожної валідації, якщо вона не виконується
// // //  * Повертати true у функції, якщо валідація проходить
// // //  * 
// // //  */

// // //  function validateEmail(promtValue) {
// // // if (promtValue.length < 5){
// // // throw new Error ('недостаточно символов')
// // // }
// // // else if (!promtValue.includes("@") ){
// // //     throw new Error ('email')
// // // } 
// // // else if (!promtValue.endsWith(".com") ){
// // //     throw new Error ('.com')
// // // } 
// // // }

// // // let promtValue = prompt()
// // // validateEmail(promtValue)


// // const user = {
// //     name: {
// //         first: 'Chester',
// //         last: 'Miller'
// //     },
// //     age: 27,
// //     skills: ['JS', 'HTML', 'CSS']
// // }

// // // get values
// // console.log(user.name);

// // console.log(user['age']);
// // // default values
// // // change naming
// // // nested objects
// // // rest operator
// // // method destructuring
// // присвоїти в змінні назву продукту, модель та ціну та 
// // вивести в консоль значення кожної створеної змінної 

// // const product = {
// //     name: 'iPhone',
// //     model: '12 Pro',
// //     company: 'Apple',
// //     price: 1400
// // }

// // const {name, model, price: productPrice, hasNFC = true} = product
// // console.log(name, model, productPrice,hasNFC);




// // const geoData = {
// //     name: 'Kyiv',
// //     type: 'city',
// //     country: 'Ukraine'
// // }

// // const company = {
// //     name: 'DAN.IT',
// //     employeesCount: 32,
// //     type: 'private'
// // }

// // const skills = {
// //     frontend: [
// //         {
// //             name: "JS",
// //             experience: 3
// //         },
// //         {
// //             name: "React + Redux",
// //             experience: 2
// //         },
// //         {
// //             name: "HTML5",
// //             experience: 2
// //         },
// //         {
// //             name: "CSS3",
// //             experience: 2
// //         },
// //     ],
// //     backend: [
// //         {
// //             name: 'PHP',
// //             experience: 2
// //         }
// //     ]
// // }
// // const {name: city} = geoData
// // const {name: companyName} = company
// // const {frontend: frontendSkills} = skills
// // const {backend: backendSkills} = skills 
// // const frontendVacancy = {city,companyName,frontendSkills}
// // const backendVacancy = {city,companyName,backendSkills}
// // console.log(frontendVacancy);
// // // console.log(backendVacancy);
// // const topCompanies = ['Apple', 'Saudi Aramco', 'Amazon', 'Microsoft', 'Alphabet'];
// // const topCompany = topCompanies[0]
// // console.log(topCompany);
// // const lowestCompany = topCompanies[topCompanies.length-1]
// // console.log(lowestCompany);

// // - присвоїти у змінні ім'я, прізвище, пошту та дату народження
// // - вивести в консоль ім'я, прізвище, пошту та РІК народження

// // const person = 'Tom, Myers, tom.myers@example.com, 8/7/1980';
// // const arr = person.split(', ')
// // console.log(arr);
// // const {name,surname,email,dateOfBirth} = arr

// // - об'єднати два об'єкта в один з назвою driver
// // - з новоствореного об'єкту отримати вік та вивести значення в консоль
// // - з новоствореного об'єкту отримати ім'я та прізвище, присвоївши їх у змінні firstName та lastName,
// // та вивести значення в консоль
// // - отримати з об'єкту driver номер посвідчення (id), яке за замовчуванням дорівнює 'sample' та 
// // вивести значення в консоль

// const person = {
//     name: {
//         first: 'Ryan',
//         last: 'Krin'
//     },
//     age: 24,
// }

// const license = {
//     type: 'B1',
//     issuedDate: '12/4/2018',
//     expirationDate: '12/4/2022'
// }
// const driver = {...person,...license,name:{...person.name},}
// const {age,name:{first:firstName,last:lastName}} = driver;

// console.log(age,firstName,lastName);
// console.log(driver.id = 'sample');
// - написати функцію getCountryByCode, яка буде знаходити країну по коду та 
// повертати всі її поля у вигляді об'єкту крім поля code
//
// - написати функцію getCapitalByName, яка буде знаходити країну по коду та
// повертати назву столиці та назву країни в форматі { city: 'Kyiv', country: 'Ukraine' }
// 
// - написати функцію getContinentByCode, яка буде повертати назву континенту по коду країни
// наприклад getContinentByCode('US') повинна повертати 'North America'
//
// - написати функцію getBiggestCountry, яка буде повертати ім'я та площу найбільшої країни у 
// вигляді об'єкту

// ADVANCED
// - написати функцію getContinentData, яка буде повертати суму всіх площ країн вказаного континенту та список 
// країн цього континенту у вигляді об'єкту { area: 10_324_532, countries: ['China', 'India'] }


const CONTINENTS = {
    NA: 'North America',
    EU: 'Europe',
    AS: 'Asia'
}

const COUNTRIES = [
    {
        code: 'US',
        name: 'United States',
        capital: 'Washington',
        area: 9_629_091,
        continent: 'NA'
    },
    {
        code: 'DE',
        name: 'Germany',
        capital: 'Berlin',
        area: 3_570_210,
        continent: 'EU'
    },
    {
        code: 'DK',
        name: 'Denmark',
        capital: 'Copenhagen',
        area: 430_940,
        continent: 'EU'
    },
    {
        code: 'UA',
        name: 'Ukraine',
        capital: 'Kyiv',
        area: 603_700,
        continent: 'EU'
    },
    {
        code: 'CN',
        name: 'China',
        capital: 'Beijing',
        area: 9_596_960,
        continent: 'AS'
    },
    {
        code: 'GB',
        name: 'United Kingdom',
        capital: 'London',
        area: 244_820,
        continent: 'EU'
    },
    {
        code: 'IN',
        name: 'India',
        capital: 'New Delhi',
        area: 3_287_590,
        continent: 'AS'
    }
];
getCountryByCode = function(code){
    const country = COUNTRIES.find(country => country.code === code);
    return country 
}
console.log(getCountryByCode());