
function createProduct(brand) {
    return function (model, price){
       return {
           brand: brand,
           model: model,
           price: price,
           getProductName(){
               console.log(`${brand},${model}`);
           }
       }
   }

}

let createApple = createProduct('apple');
let createSamsung = createProduct('samsung');
let createXiaomi = createProduct('xiaomi');

let iphone12 = createApple('Iphone12', 2000); // { brand: 'Apple', model: 'Iphone12', price: 2000}
let iphone10 = createApple('Iphone10', 1500);
let iphone6 = createApple('Iphone6', 1000);

console.log(iphone12);