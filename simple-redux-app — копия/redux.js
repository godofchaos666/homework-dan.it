console.log(Redux);
const todos = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODO': {
            return [...state, action.payload]
        }
        case 'REMOVE_TODO': {
            return state.filter(todo => todo.id !== action.payload)
        }
        case 'COMPLETED_TODO':{

        }
        default: {
            return state
        }
    }
}
const initialState = [{ id: Date.now(), body: 'Learn Redux', isCompleted:false }, { id: Date.now() + 1, body: 'Learn Router',isCompleted:false }]
const store = Redux.createStore(
    todos,
    initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

console.log(store);

store.subscribe(() => {
    const state = store.getState()
    console.log(state);
});

class Title {
    constructor(id) {
        this.el = document.querySelector(id)
        this.render()
        store.subscribe(() => this.render())
    }
    render() {
        const todos = store.getState()
        // console.log(todos);
        const count = todos.length
       
let completed = todos.filter(todo => todo.isCompleted === false)

        this.el.innerText = `Tasks (${count})`
        document.querySelector(`.all`).innerText = `All (${count})`
        document.querySelector('#toDo').innerText = `To Do (${completed.length})`
    }
}

class TaskList {
    constructor(elemId, onRemove) {
        this.listEl = document.querySelector(elemId);
        this.onRemove = onRemove;
        this.attachListeners();
        this.render()
        store.subscribe(() => this.render())
    }

    attachListeners() {
        this.listEl.addEventListener('click', (event) => {
            
            if (event.target.classList.contains('js-remove-todo')) {
                this.onRemove(+event.target.closest('li').dataset.id)
            }
            if (event.target.classList.contains('task')){
             event.target.classList.toggle('completed');
             store.dispatch({
                type: 'COMPLETED_TODO',
                payload: `isCompleted`
            })
            }
        })
    }

    render() {
        const tasks = store.getState()
        const list = tasks.map(task => `
            <li  data-id="${task.id}">
                <span class='task'>${task.body}</span>
                <button class="js-remove-todo">X</button>
            </li>
        `)
        this.listEl.innerHTML = list.join('')
    }
}

class Form {
    constructor() {
        this.form = document.querySelector('#form')
        this.title = new Title('#heading')
        this.list = new TaskList('#task-list', this.handleRemove.bind(this))
     
        this.allButton = document.querySelector('.all')
        this.toDoButton = document.querySelector('#toDo')
        this.completedButton = document.querySelector('#completed')
        this.attachListeners()
    }

    handleRemove(id) {
        store.dispatch({
            type: 'REMOVE_TODO',
            payload: id
        })
    }

    attachListeners() {
        this.form.addEventListener('submit', (event) => {
            event.preventDefault()
            const input = event.target.elements.task
            const { value } = input
            if (!value) return
            const newTask = { id: Date.now(), body: value,isCompleted: false }

            store.dispatch({
                type: 'ADD_TODO',
                payload: newTask
            })

            input.value = ''
        })
     this.allButton.addEventListener('click',function(){
             
            })
        this.toDoButton.addEventListener('click',function(){
;
        })
        this.completedButton.addEventListener('click',function(){

        })
    }
}

const taskManager = new Form()
console.log(store.getState());