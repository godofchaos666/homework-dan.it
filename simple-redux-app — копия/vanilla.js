
class Title {
    constructor(id) {
        this.el = document.querySelector(id)
    }
    render(count) {
        this.el.innerText = `Tasks (${count})`
    }
}

class TaskList {
    constructor(elemId, onRemove) {
        this.listEl = document.querySelector(elemId);
        this.onRemove = onRemove;
        this.attachListeners();
    }

    attachListeners() {
        this.listEl.addEventListener('click', (event) => {
            console.log(event);
            if (event.target.classList.contains('js-remove-todo')) {
                this.onRemove(+event.target.closest('li').dataset.id)
            }
        })
    }

    render() {
        const tasks = store.getState()
        const list = tasks.map(task => `
            <li data-id="${task.id}">
                <span>${task.body}</span>
                <button class="js-remove-todo">X</button>
            </li>
        `)
        console.log(list);
        this.listEl.innerHTML = list.join('')
    }
}

class Form {
    constructor() {
        this.tasks = []

        this.form = document.querySelector('#form')
        this.title = new Title('#heading')
        this.list = new TaskList('#task-list', this.handleRemove.bind(this))
        this.attachListeners()
    }

    handleRemove(id) {
        console.log(id);
        this.tasks = this.tasks.filter(task => task.id !== id)
        console.log(this.tasks);
        this.title.render(this.tasks.length)
        this.list.render(this.tasks)
    }

    attachListeners() {
        this.form.addEventListener('submit', (event) => {
            event.preventDefault()
            const input = event.target.elements.task
            const { value } = input
            if (!value) return
            const newTask = { id: Date.now(), body: value }
            this.tasks.push(newTask)
            input.value = ''
            this.title.render(this.tasks.length)
            this.list.render(this.tasks)
        })
    }
}

const taskManager = new Form()
