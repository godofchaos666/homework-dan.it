let images = document.querySelectorAll('.image-to-show');
let imagesWrapper = document.querySelector('.images-wrapper');
let index = 0;

let slider = setInterval(() => {
    index += 1
    if (index === images.length) {
        index = 0;
    }
    imagesWrapper.style.transform = `translate3d(${index * -1200}px, 0, 0)`;
}, 3000);


let stop = document.querySelector('.stop')
stop.addEventListener('click', () => {
    clearInterval(slider);
})

let start = document.querySelector('.restart');
start.addEventListener('click', () => {
    setInterval(() => {
        index += 1
        if (index === images.length) {
            index = 0;
        }
        imagesWrapper.style.transform = `translate3d(${index * -1200}px, 0, 0)`;
    }, 3000);
})

