'use strict';
const input = document.createElement('input');
input.placeholder = 'Price'
document.body.append(input);

const span = document.createElement('span');
span.textContent = ''

const btn = document.createElement('button')
btn.textContent = 'X'

const spanError = document.createElement('span')
spanError.textContent = 'Please enter correct price'

const focusFunction = function(){
    input.className = ' focus';
}
input.addEventListener('focus',focusFunction)

input.addEventListener('blur',function(){
   
    if (+input.value < 0){
      span.remove()
      btn.remove()
document.body.append(spanError)
input.style.color = 'black'
input.className = ' error'
    }
    else if (input.value !== "" && !isNaN(+input.value) && +input.value >= 0) {
        spanError.remove()
    input.className = ' unfocus';
span.textContent = `Текущая цена: ${input.value}`
input.style.color = 'green'
input.before(span);
input.before(btn);
    }
    else if (!input.value){
        input.className = ' unfocus'
        btn.remove()
    spanError.remove()
    span.remove()
    }
}) 

btn.addEventListener('click',function(){
    span.remove()
    btn.remove()
    input.value = ''
})
