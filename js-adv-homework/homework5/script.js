
const urlUsers = 'https://ajax.test-danit.com/api/json/users'
const urlPosts = 'https://ajax.test-danit.com/api/json/posts'



createElement = function({tagName = 'div',text = '',className = ''}){
    const element = document.createElement(tagName)
    //
    // element.classList.add(className)   Unhandled Promise Rejection: SyntaxError: The string did not match the expected pattern. ????
    element.textContent = text
    return element
}
class Card {
    constructor(title,body,userId,id){
        this.title = title;
        this.body = body;
       this.userId = userId;
       this.id = id;
    }
 
 render(){
     const cardContainer = createElement({
         tagName: 'div',
         text: '',
    className: 'card-container'
        })
        cardContainer.classList.add('card-container')
     const cardTitle = createElement({
             tagName: 'h2',
             text: this.title, 
             className: 'card-title'
            })
            cardTitle.classList.add('card-title')
     const cardBody = createElement({
                tagName: 'p', 
                text: this.body,
                className: ''
            })
          const cardButton = createElement({
              tagName: 'button',
              text: 'DELETE'
          })
           
            fetch(`${urlUsers}/${this.userId}`)
            .then(response => response.json())
            .then(element => {
                const UserName = document.createElement('h2')
                UserName.classList.add('card-name');
                const UserEmail = document.createElement('span')
                UserEmail.classList.add('email');
                UserEmail.textContent = element.email
                UserName.textContent = `${element.name} `
                UserName.append(UserEmail)
                cardContainer.prepend(UserName)

            } )
 cardContainer.append(cardTitle,cardBody,cardButton)
 document.body.append(cardContainer)
 cardButton.addEventListener('click', function() {
        fetch(`${urlPosts}/${this.id}`, {
            method: 'DELETE',
         
        })
        cardContainer.remove()
    })
    
 
 }
}
fetch(urlPosts)
.then(response => response.json())
.then(element => {
    element.forEach(element => {
        element = new Card(element.title,element.body,element.userId,element.id)
        element.render()
      
    })
})

