const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",

    }
];


const root = document.getElementById('root');
const list = document.createElement('ul');
books.forEach((el) => {
    const {author, name, price} = el;
    if (author && name && price) {
        list.insertAdjacentHTML("afterbegin", `<li>Автор: ${author}, Название: ${name}, Стоимость: ${price}</li>`);
        root.append(list)
    }
})
function checkObject(obj) {
    const {author, name, price} = obj
    if (!author && !name && !price) {
      throw new Error('Нет автора, нет названия, нет цены')
    } else if (!name && !price) {
        throw  new Error("Нет названия, нет цены")
    } 
     else if (!author && !price) {
    throw  new Error("Нет автора, нет цены")
     }
     else if (!author && !name) {
        throw  new Error("Нет автора, нет названия")
         }
      else if (!price) {
        throw  new Error("Нет цены")
    }
    else if (!author) {
        throw new Error("Нет автора")
    }
    else if (!name){
        throw new Error("Нет названия")
    }
    
}

function check(arr) {
    for (let i = 0; i < arr.length; i++) {
        try{
            checkObject(arr[i])
        }
        catch (e){
            console.error(e)
        }
    }
}



check(books)

