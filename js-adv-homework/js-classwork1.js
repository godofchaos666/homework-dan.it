/**
 * Наявний уже функціонал для одного складу (kyivWarehouse)
 * Але з розвитком компанії з'явилося ще 2 склади (lvivWarehouse та kharkivWarehouse)
 * відповідно з місткістю 50 та 40 та поки що без продуктів
 * 
 * Відповідно потрібно зберегти логіку, але додати можливість також
 * масштабувати її
 * Для цього необхідно переробити об'єкт в функцію-конструктор з методами
 * та створити відповідно 3 склади
 * 
 
 * - додати на склад Львову 7 телефонів, 10 планшетів
 * - додати на склад Харькова 10 телефонів та 6 ноутбуків
 * - вивести загальну кількість товарів по кожному складу в консоль
 * - вивести всі товари кожного складу в консоль
 * - дізнатися, якого товару найбільше у Львові
 * 
 ADVANCED:
 - створити окремо функцію getTotalQuantity, якій можна передати назву товару
 та отримати загальну кількість товарів по всіх складах
 - додати можливість переміщення товарів між складами
 для цього додати метод moveProduct(name, quantity, targetWarehouse)
 - перемістити 4 ноутбуки з Києва у Львів
 * 
 */

 class Device {
     constructor(brand,model,price){
         this.brand = brand;
         this.model = model; 
         this.price = price;
     }
     getName(){
    console.log(`${this.brand} ${this.model}`);
     }
     storePrice(){
  return      (this.price * 20 / 100) + this.price
     }
 }
 class laptop extends Device {
    constructor(brand,model,price) {
        super(brand,model,price)
    }
   deliveryCharge() {
       if (this.price > 30000){
    console.log(0);;
       }
       else {
     console.log(200);;
       }
   }
 }
 class phone extends Device {
     constructor(brand,model,price,NFC){
         super(brand,model,price)
         this.NFC = NFC;
     }
     deliveryCharge() {
        if (this.storePrice > 10000){
       console.log(this.price / 100);;
        }
        else {
           console.log(this.price * 3 / 100);
        }
    }
 }
 class tablet extends Device {
     constructor(brand,model,price,SIM){
         super(brand,model,price)
         this.SIM = SIM;
     }
     deliveryCharge() {
        if (this.price > 20000){
     console.log(0);
        }
        else {
 return this.storePrice;
        }
    }
 }
const mac = new laptop ('Apple','MBP2015','40000')
mac.deliveryCharge()
const galaxy = new tablet ('samsung','galaxy','10000')
galaxy.storePrice()