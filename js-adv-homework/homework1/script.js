class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
}
get fullName() {
    return this.name
}
 set fullName(value){
     return this.name = value;
 } 
 get age() {
     return this.age
 }
 set age(value){
     return this.age = value;
 }
 get salary() {
     return this.salary;
 }
 set salary(value){
     return this.salary = value;
 }
}
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang
    }
 get salary(){
       return `${this.salary * 3}`;
   }
   
   }

const Anton = new Programmer ('Anton','19',"500",)
const Vasya = new Employee ('Vasya','20','1000')

console.log(Anton);
console.log(Anton.salary());


console.log('----------------');
console.log(Vasya);
console.log(Vasya.getSalary);
