import React from 'react';
 class Button extends React.Component {
    constructor(props){
      super(props)
      this.state={
        modalOpen: false,
        }
    }
    render() {
      return(
        <button style={{
          backgroundColor: `${this.props.backgroundColor}`,
        }} onClick={this.props.onClick}>{this.props.text}</button>
      )
    }
    }
    export default Button;