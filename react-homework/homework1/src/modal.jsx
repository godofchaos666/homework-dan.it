import React from 'react';
 class Modal extends React.Component {
    constructor(props){
      super(props)
      this.state={
        modalOpenFirst: false,
    
        }
    }
  
    
    render(){
      return(
        <>

        { this.props.isOpen&&
        <div style={{
          backgroundColor: `${this.props.mainBackgroundColor}`
        }} className="modal-body">
  <div style={{
          backgroundColor: `${this.props.headerBackgroundColor}`
        }} className="modal-header">
    <h1 className="modal-title">
      {this.props.modalTitle}
  {this.props.closeButton? <img className="modal-cross" onClick={this.props.onclose}  src='./cross.png'/> : <></>}
    </h1>
  </div>
  <p className="modal-text">
  {this.props.modalText}
  </p>
  <p className="modal-sub-text">{this.props.modalSubText} </p>
  {this.props.action}
        </div>
    }
        </>
      )
    }
  }
  export default Modal