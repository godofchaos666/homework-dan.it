import React,{Component,useState} from 'react';
import './App.css';
import Modal from './modal.jsx';
import AppBody from './appbody.jsx';
function App() {
  
  
  return (
    
    <div className="App">
      <header className="App-header">
        <AppBody/>
      </header>
    </div>
  );
}

export default App;
