import React from 'react';
import Button from './button.jsx'
import Modal from './modal.jsx';
import List from './list.jsx';
class AppBody extends React.Component {
    constructor(){
      super()
      this.state={
      modalOpenFirst: false,
      modalOpenSecond: false,
      }
    }
    
    openFirstModal = () => {
    this.setState({modalOpenFirst:true})
    document.getElementById('overlay').classList.add('bg_layer')
    }
    openSecondModal = () => {
      this.setState({modalOpenSecond:true})
      document.getElementById('overlay').classList.add('bg_layer')
    }
    closeModal = () => {
      console.log(1);
            this.setState({modalOpenFirst:false})
            this.setState({modalOpenSecond:false})
            document.getElementById('overlay').classList.remove('bg_layer')
          }
    render (){
  return(
    <><div id="overlay">
      <List/>
      
         </div>
         {this.state.modalOpenFirst? (<Modal mainBackgroundColor='rgba(255, 17, 0, 0.705)'  headerBackgroundColor='red' modalTitle='Do you want to delete this file?' modalText='Once you delete this file, it won’t be possible to undo this action.' modalSubText='Are you sure you want to delete it?'  onclose={this.closeModal}  closeButton={true}
         action={<div className="modal-buttons-container"><button className='button'>Ok</button>
         <button className='button' >Cancel</button></div>}
         />) : <></>}
          {this.state.modalOpenSecond? (<Modal mainBackgroundColor='green' headerBackgroundColor='yellow' modalTitle='Do to add file?' modalText='Once you delete this file, it won’t be possible to undo this action.' modalSubText='Are you sure you want to delete it?' onclose={this.closeModal} closeButton={true}
         action={<div className="modal-buttons-container"><button  className='button'>Ok</button>
         <button className='button'>Cancel</button></div>}
         />) : <></>}
          
    </>
  )
    }
  }
  export default AppBody