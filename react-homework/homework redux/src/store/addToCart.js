const defaultStore = {
    cart: []
}
export  const cartReducer = (state=defaultStore,action) => {
    switch (action.type){
        case "ADD": 
        return {...state,cart:[...state.cart,action.payload]}
        case "REMOVE":
        return {...state,cart: state.cart.filter(item => item.article !== action.payload)}
        // case "MODAL_CLOSE":
        //   return {...state,modalOpen:false}
        default: return state
      }
}
