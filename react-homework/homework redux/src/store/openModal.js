const defaultState = {
    modalOpen: false
  }
  

const reducer = (state = defaultState,action) => {
    switch (action.type){
      case "MODAL_OPEN": 
      return {...state,modalOpen:action}
      // case "MODAL_CLOSE":
      //   return {...state,modalOpen:false}
      default: return state
    }
}
export default reducer