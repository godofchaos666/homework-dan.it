const defaultStore = {
    favor: []
}
export  const favorReducer = (state=defaultStore,action) => {
    switch (action.type){
        case "ADD": 
        return {...state,favor:[...state.favor,action.payload]}
        case "REMOVE":
        return {...state,favor: state.favor.filter(item => item.article !== action.payload)}
        // case "MODAL_CLOSE":
        //   return {...state,modalOpen:false}
        default: return state
      }
}
