import {createStore,combineReducers} from 'redux'
import reducer from './openModal.js'
import {favorReducer} from './addToFavor.js'
import {composeWithDevTools} from 'redux-devtools-extension';
import { cartReducer } from './addToCart.js';
import {allReducer} from './allItems.js'
const rootReducer = combineReducers({
    all: allReducer,
    modal: reducer,
favor: favorReducer,
cart: cartReducer
})
const store = createStore(rootReducer,composeWithDevTools())
store.subscribe(() => {
    localStorage['redux-store'] = JSON.stringify(store.getState())
})
export default store 