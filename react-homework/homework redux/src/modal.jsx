import React from 'react';
 const Modal = props => {

  

  
      return(
        <>{
        <div style={{
          backgroundColor: 'rgba(255, 17, 0, 0.705)'  
        }} className="modal-body">
  <div style={{
          backgroundColor: `${props.headerBackgroundColor}`
        }} className="modal-header">
    <h1 className="modal-title">
      {props.modalTitle}
  {props.closeButton? <img className="modal-cross" onClick={props.onclose}  src='./cross.png'/> : <></>}
    </h1>
  </div>
  <p className="modal-text">
  {props.modalText}
  </p>
  <p className="modal-sub-text">{props.modalSubText} </p>
  {props.action}
        </div>
       }
        </>
      )
    }
  
  export default Modal