import React,{useState} from 'react';

import Modal from './modal.jsx';
import List from './list.jsx';
import Cart from './favorites.jsx';
import {useDispatch,useSelector} from 'react-redux';


const AppBody = () => {
  const dispatch = useDispatch();
  const modalOpen = useSelector(state => state.modal.modalOpen.payload)
  console.log(modalOpen);
  const openModal = () => {
 dispatch({type:'MODAL_OPEN',payload:true});
 document.getElementById('overlay').classList.toggle('bg_layer')
  }
  const closeModal = () => {
    dispatch({type:'MODAL_OPEN',payload:false});
    document.getElementById('overlay').classList.toggle('bg_layer')
  }

    
  return(
    <><div id="overlay">

      <List handleClick={openModal}></List> 
         </div>
       {modalOpen ?  <Modal mainBackgroundColor='rgba(255, 17, 0, 0.705)'  headerBackgroundColor='red' modalTitle='Add to cart?'  onclose={closeModal}   closeButton={true} 
               action={<div className="modal-buttons-container"><button 
               className='button'>Ok</button>
               <button className='button' >Cancel</button></div>}
               /> : <></>}
    </>
  )
  }
  
  export default AppBody