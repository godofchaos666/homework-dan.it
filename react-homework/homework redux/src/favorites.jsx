import React from "react";
import Item from "./item.jsx";
import {useState,useEffect} from 'react'
import axios from "axios";
import {NavLink} from 'react-router-dom';
import {useDispatch,useSelector} from 'react-redux';
const Favorites = (props) => {
    const data = localStorage.getItem('redux-store')
let parsedData = JSON.parse(data)
 let favorites = parsedData.favor

    return(
        <>
        <NavLink to='/appbody'>Main</NavLink>
        <ul className="posts-list">
        {favorites.favor.map(item => <Item imgUrl={item.imgUrl}  name={item.name} price={item.price} key={item.article} article={item.article} handleClick={item.handleClick} isFavorited={false} ></Item>)}
        </ul >
            
        </>
    )
}
export default Favorites