import React,{useState,useEffect} from 'react';
import Modal from './modal.jsx'
import {useDispatch,useSelector} from 'react-redux';

function Item(props ){
    const dispatch = useDispatch();
    const favor = useSelector(state => state.favor)
const addCustomer = () => {
    const item = {
        imgUrl: props.imgUrl,
        name: props.name,
        price: props.price,
        handleClick: props.handleClick,
        article: props.article
        
    }
    dispatch({type:'ADD',payload:item})
}
const removeCustomer = (article) => {
    dispatch({type:'REMOVE',payload:article})
}

const [isFavorited,setFavorite] = useState(false);
const [reduxFavorited,setReduxFavorited] = useState(null)

    const FavoriteItem = function(){
if (isFavorited){
    removeCustomer(props.article);
    setFavorite(false);

    // localStorage.setItem(`isFavorited${props.article}`,!isFavorited)
}
else {
    addCustomer()
    setFavorite(true);
    // localStorage.setItem(`isFavorited${props.article}`,!isFavorited)
    
}

    }
useEffect(()=>{
    const raw = localStorage.getItem(`isFavorited${props.article}`) || []
    setFavorite(JSON.parse(raw))
},[])
    useEffect(()=>{
 
        localStorage.setItem(`isFavorited${props.article}`,isFavorited)
        },[isFavorited])
    
     /// redux
    
    


     /////
        return(
  <>
            <div className="item-body" style={{
                display: props.display
            }} >
<img src={props.imgUrl} className="item-img" alt="" />

{/* |||| localStorage.getItem(`isFavorited${props.article}`) == `true`   */}
 {isFavorited ?  <img  src='./img/heart-black.png' onClick={FavoriteItem}className='item-favourite'/> :
  <img  src="./img/heart.svg" alt="" className="item-favourite" onClick={FavoriteItem}  /> }


<h2 className="item-title" >{props.name}</h2>
<h2 className="item-price" >{props.price}</h2>
<img src='./img/addtocart.png' className='item-cart'  onClick={props.handleClick}></img>
            </div>
           
  </>
                
        )
    }

export default Item 