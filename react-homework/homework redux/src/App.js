import React,{Component,useState} from 'react';
import './App.css';
import Modal from './modal.jsx';
import AppBody from './appbody.jsx';
import {Route,BrowserRouter,Switch,Redirect,withRouter} from 'react-router-dom';
import Favorites from './favorites.jsx';
import Cart from './cart.jsx';
function App(props) {
  
  
  return (

    <div className="App">
      <header className="App-header">
    <Switch>
<Route exact path='/'>
  <Redirect to='/appbody'></Redirect>
</Route>
        <Route exact path="/appbody" component={AppBody}/>

        <Route path='/favorites' component={Favorites}/>
        <Route path='/cart' component={Cart}/>
    </Switch>
      </header>
    </div>
   
  );
}

export default withRouter(App); 
