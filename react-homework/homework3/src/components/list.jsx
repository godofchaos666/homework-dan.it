import React from "react";
import Item from "./shopItem.jsx";
import {useState,useEffect} from 'react'
import axios from "axios";
import {NavLink} from 'react-router-dom';
const List = props => {
        const [posts,setPosts]=useState([]);
         useEffect(() => {
             axios.get('./index.json')
             .then(res => {
     
                 setPosts(res.data)
             })
         },[])
    
    return(
        
        <>
        <div className="body">
            <h2 style={{
                textAlign: 'center',
                fontSize: '30px',
            }}>Latest Arrival</h2>
            <NavLink to='/favorites'>Favorites</NavLink>
            <NavLink to='/cart'>Cart</NavLink>
            <ul className="posts-list">
                {posts.map(post => <Item imgUrl={post.imgUrl}  name={post.name} price={post.price} key={post.article} article={post.article} handleClick={props.handleClick} isFavorited={false} ></Item>)}
            </ul>
        </div>

        </>
    )
}

export default List;