import React from 'react';
import Modal from './modal.jsx'
import Button from './button.jsx'
let favoriteList = []

class Item extends React.Component {
    constructor() {
        super()
this.state = {
    isFavorited: false,
}

    }
    FavoriteItem = (id) => {
      if (!favoriteList.includes(id)) {
        favoriteList.push(id)
        localStorage.setItem("favoriteList",JSON.stringify(favoriteList));
        
      }
     else {
         favoriteList.forEach(item => {
            if (item = id){
                favoriteList.push(item)
            }
         })
         localStorage.setItem("favoriteList",JSON.stringify(favoriteList));
     }
      
    }
  
    
    render(){

        return(
            <div className="item-body" >
<img src={this.props.imgUrl} className="item-img" alt="" />
{localStorage.getItem("favoriteList").includes(this.props.article)? <img onClick={()=>  {
    this.FavoriteItem(this.props.article)

}} src='./img/heart-black.png' className='item-favourite'/> : <img onClick={()=>  {
    this.FavoriteItem(this.props.article)
}} src="./img/heart.svg" alt="" className="item-favourite" /> }
<h2 className="item-title" >{this.props.name}</h2>
<h2 className="item-price" >{this.props.price}</h2>
<Button ></Button>
            </div>
        )
    }
}
export default Item 