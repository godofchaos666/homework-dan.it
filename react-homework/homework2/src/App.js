
import './App.css';
import List from './list.jsx';
import {useState,useEffect} from 'react'
import Modal from './modal.jsx';
import React from 'react'
class App extends React.Component  {
  constructor(){
    super()
    this.state ={
      modalOpen: false
    }
  }
 render(){
  return (
    <div className="App">
<List/>
{this.state.modalOpen? <Modal></Modal> : <></>}
    </div>
  );
}
}

export default App;
