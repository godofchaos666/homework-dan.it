import React from "react";
import Item from "./item.jsx";
import {useState,useEffect} from 'react'
import axios from "axios";
function List() {
        const [posts,setPosts]=useState([]);
         useEffect(() => {
             axios.get('./index.json')
             .then(res => {
                 console.log(res);
                 setPosts(res.data)
             })
         },[])

        
    return(
        <>
        <div className="body">
            <h2 style={{
                textAlign: 'center',
                fontSize: '30px',
            }}>Latest Arrival</h2>
            <ul className="posts-list">
                {posts.map(post => <Item imgUrl={post.imgUrl}  name={post.name} price={post.price} key={post.article} article={post.article}  ></Item>)}
            </ul>
        </div>

        </>
    )
}

export default List;