
import Item from "./shopItem.jsx";

import axios from "axios";
import {NavLink} from 'react-router-dom';
import React,{useState,useEffect} from 'react';
import Modal from './modal.jsx'

const Cart = (props) => {
    const [cartArray,setCartArray] = useState(JSON.parse(localStorage.getItem('cartArray')) || [])
    const [isinCart,setCart] = useState(true);
    const [modalOpenFirst,setModalOpen] = useState(false);
    const openFirstModal = () => {
      if(modalOpenFirst){
        setModalOpen(false)

      }
      else {
       setModalOpen(true)
      
      }
     }
     const deleteFromCart = () => {

        if (isinCart){
           
      return
        }  
      
      }    
         useEffect(() => {
      if (JSON.parse(localStorage.getItem('cartArray'))?.includes(props.article)){
          setCart(true)
      }
      else {
          setCart(false)
      }
         },[])
    const [posts,setPosts]=useState([]);
         useEffect(() => {
             axios.get('./index.json')
             .then(res => {
                 console.log(res);
                 setPosts(res.data)
             })
         },[])
     let array = posts.filter(post =>  JSON.parse(localStorage.getItem('cartArray'))?.includes(post.article))
     console.log(array);
    return(
        <>
        <NavLink to='/appbody'>Main</NavLink>
        <ul className="posts-list">
        {array?.map(post =>  < Item imgUrl={post.imgUrl}  name={post.name} price={post.price} key={post.article} article={post.article} handleClick={props.handleClick} isFavorited={false} ></Item>)}

            </ul>
            <Modal mainBackgroundColor='rgba(255, 17, 0, 0.705)'  headerBackgroundColor='red' modalTitle='Delete from cart?'  onclose={openFirstModal}  isOpen={modalOpenFirst} closeButton={true} 
               action={<div className="modal-buttons-container"><button   className='button'>Ok</button>
               <button  className='button' >Cancel</button></div>}
               />
        </>
    )
}
export default Cart